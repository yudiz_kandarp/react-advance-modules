import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.scss'
import Navbar from './Components/Navbar/Navbar'
import HelmetPage from './Pages/HelmetPage'
import SingleUser from './Pages/SingleUser'
import TablePage from './Pages/TablePage'
import Table from './Components/Tables/Table'
import SortingTable from './Components/Tables/SortingTable'
import FilteringTable from './Components/Tables/FilteringTable'
import PaginationTable from './Components/Tables/PaginationTable'
import ColumnHiding from './Components/Tables/ColumnHiding'
import QueryPage from './Pages/QueryPage'

function App() {
	return (
		<BrowserRouter>
			<Navbar />
			<Routes>
				<Route path='/' element={<HelmetPage />} />
				<Route path='/query' element={<QueryPage />} />
				<Route path='/table' element={<TablePage />}>
					<Route path='normaltable' element={<Table />} />
					<Route path='sort' element={<SortingTable />} />
					<Route path='filter' element={<FilteringTable />} />
					<Route path='pagination' element={<PaginationTable />} />
					<Route path='columnhiding' element={<ColumnHiding />} />
				</Route>
				<Route path='/helmet/:id' element={<SingleUser />} />
			</Routes>
		</BrowserRouter>
	)
}

export default App
