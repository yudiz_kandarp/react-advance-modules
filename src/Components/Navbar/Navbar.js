import React from 'react'
import { Link } from 'react-router-dom'
import './navbar.scss'
function Navbar() {
	return (
		<header>
			<div className='navbar'>
				<h2>Modules</h2>
				<div className='nav'>
					<Link to='/'>Helmet</Link>
					<Link to='/table/normaltable'>Table</Link>
					<Link to='/query'>Query</Link>
				</div>
			</div>
		</header>
	)
}

export default Navbar
