import React from 'react'
import PropTypes from 'prop-types'
function ColumnFilter({ column }) {
	const { filterValue, setFilter } = column
	return (
		<span>
			<input
				value={filterValue || ''}
				onChange={(e) => {
					setFilter(e.target.value)
				}}
				placeholder={`Search`}
				style={{
					fontSize: '1.1rem',
					marginTop: '20px',
					border: '1px solid gray',
				}}
			/>
		</span>
	)
}
ColumnFilter.propTypes = {
	column: PropTypes.object,
}
export default ColumnFilter
