import React from 'react'
import PropTypes from 'prop-types'
function ColumnFilterSelect({ column }) {
	const { filterValue, setFilter } = column
	return (
		<span>
			<select
				value={filterValue || ''}
				name='filter'
				id=''
				onChange={(e) => setFilter(e.target.value)}
				style={{
					fontSize: '1.1rem',
					marginTop: '20px',
					border: '1px solid gray',
				}}
			>
				<option value=''>select value</option>
				<option value='Male'>Male</option>
				<option value='Female'>Female</option>
				<option value='Polygender'>Polygender</option>
				<option value='Non-binary'>Non-binary</option>
				<option value='Genderqueer'>Genderqueer</option>
			</select>
		</span>
	)
}
ColumnFilterSelect.propTypes = {
	column: PropTypes.object,
}
export default ColumnFilterSelect
