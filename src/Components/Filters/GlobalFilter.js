import React from 'react'
import PropTypes from 'prop-types'

function GlobalFilter({ globalFilter, setGlobalFilter }) {
	// const onChange = useAsyncDebounce((value) => {
	// 	setGlobalFilter(value)
	// }, 200)

	return (
		<span>
			Search:{' '}
			<input
				value={globalFilter || ''}
				onChange={(e) => {
					setGlobalFilter(e.target.value)
				}}
				placeholder={`Search`}
				style={{
					fontSize: '1.1rem',
					marginTop: '20px',
					border: '1px solid gray',
				}}
			/>
		</span>
	)
}
GlobalFilter.propTypes = {
	globalFilter: PropTypes.any,
	setGlobalFilter: PropTypes.func,
}

export default GlobalFilter
