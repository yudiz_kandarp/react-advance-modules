import React from 'react'
import { useFilters, useGlobalFilter, useTable } from 'react-table'
import Data from '../../Data/TABLE_DATA.json'
import ColumnFilter from '../Filters/ColumnFilter'
import ColumnFilterSelect from '../Filters/ColumnFilterSelect'
import GlobalFilter from '../Filters/GlobalFilter'

function FilteringTable() {
	const columns = React.useMemo(
		() => [
			{ Header: 'Id', accessor: 'id', Filter: ColumnFilter },
			{
				Header: 'Name',
				columns: [
					{
						Header: 'First Name',
						accessor: 'first_name',
						Filter: ColumnFilter,
					},
					{
						Header: 'Last Name',
						accessor: 'last_name',
						Filter: ColumnFilter,
					},
				],
			},
			{
				Header: 'Info',
				columns: [
					{
						Header: 'Email',
						accessor: 'email',
						Filter: ColumnFilter,
					},
					{
						Header: 'Gender',
						accessor: 'gender',
						Filter: ColumnFilterSelect,
					},
				],
			},
		],
		[]
	)
	const data = React.useMemo(() => Data, [])
	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		rows,
		prepareRow,
		state,
		setGlobalFilter,
	} = useTable(
		{
			columns,
			data,
		},
		useFilters,
		useGlobalFilter
	)
	const { globalFilter } = state

	return (
		<>
			<GlobalFilter
				globalFilter={globalFilter}
				setGlobalFilter={setGlobalFilter}
			/>
			<table {...getTableProps()}>
				<thead>
					{headerGroups.map((headerGroup, i) => (
						<tr key={i} {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column, i) => (
								<th key={i} {...column.getHeaderProps()}>
									{column.render('Header')}
									<div>
										{column.canFilter ? column.render('Filter') : null}{' '}
									</div>
								</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()}>
					{rows.map((row, i) => {
						prepareRow(row)
						return (
							<tr key={i} {...row.getRowProps()}>
								{row.cells.map((cell, i) => {
									return (
										<td {...cell.getCellProps()} key={i}>
											{cell.render('Cell')}
										</td>
									)
								})}
							</tr>
						)
					})}
				</tbody>
			</table>
		</>
	)
}

export default FilteringTable
