import React from 'react'
import { useSortBy, useTable } from 'react-table'
import Data from '../../Data/TABLE_DATA.json'

function SortingTable() {
	const columns = React.useMemo(
		() => [
			{
				Header: 'Name',
				columns: [
					{
						Header: 'First Name',
						accessor: 'first_name',
					},
					{
						Header: 'Last Name',
						accessor: 'last_name',
					},
				],
			},
			{
				Header: 'Info',
				columns: [
					{
						Header: 'Email',
						accessor: 'email',
					},
					{
						Header: 'Gender',
						accessor: 'gender',
					},
				],
			},
		],
		[]
	)

	const data = React.useMemo(() => Data, [])

	const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
		useTable(
			{
				columns,
				data,
			},
			useSortBy
		)

	return (
		<table {...getTableProps()}>
			<thead>
				{headerGroups.map((headerGroup, i) => (
					<tr key={i} {...headerGroup.getHeaderGroupProps()}>
						{headerGroup.headers.map((column, i) => (
							<th
								key={i}
								{...column.getHeaderProps(column.getSortByToggleProps())}
							>
								{column.render('Header')}
								<span>
									{column.isSorted ? (column.isSortedDesc ? ' ⏬' : ' ⏫') : ''}
								</span>
							</th>
						))}
					</tr>
				))}
			</thead>
			<tbody {...getTableBodyProps()}>
				{rows.map((row, i) => {
					prepareRow(row)
					return (
						<tr key={i} {...row.getRowProps()}>
							{row.cells.map((cell, i) => {
								return (
									<td {...cell.getCellProps()} key={i}>
										{cell.render('Cell')}
									</td>
								)
							})}
						</tr>
					)
				})}
			</tbody>
		</table>
	)
}

export default SortingTable
