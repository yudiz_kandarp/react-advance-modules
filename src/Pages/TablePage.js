import React from 'react'
import { Helmet } from 'react-helmet'
import { Outlet, useNavigate, useLocation } from 'react-router-dom'
import Button from '../Components/Button/Button'

function TablePage() {
	const location = useLocation()
	const navigate = useNavigate()
	function isActive(path) {
		return location.pathname == path
	}
	return (
		<>
			<div className='table_nav'>
				<Button
					active={isActive('/table/normaltable')}
					onClick={() => navigate('/table/normaltable')}
				>
					Table
				</Button>
				<Button
					active={isActive('/table/sort')}
					onClick={() => navigate('/table/sort')}
				>
					sorting Table
				</Button>
				<Button
					active={isActive('/table/filter')}
					onClick={() => navigate('/table/filter')}
				>
					filtering Table
				</Button>
				<Button
					active={isActive('/table/pagination')}
					onClick={() => navigate('/table/pagination')}
				>
					pagination Table
				</Button>
				<Button
					active={isActive('/table/columnhiding')}
					onClick={() => navigate('/table/columnhiding')}
				>
					Column Hiding Table
				</Button>
			</div>
			<div>
				<Outlet />
			</div>
			<Helmet>
				<title>Modules - Table</title>
			</Helmet>
		</>
	)
}

export default TablePage
