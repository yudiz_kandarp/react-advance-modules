import React from 'react'
import { Helmet } from 'react-helmet'
import { useNavigate } from 'react-router-dom'
import data from '../Data/data_helmet.json'
function HelmetPage() {
	let navigate = useNavigate()
	return (
		<div className='Helmet_container'>
			<div className='user_list'>
				{data.map((item) => (
					<div
						key={item.id}
						onClick={() => navigate(`/helmet/${item.id}`)}
						className='user_data'
					>
						<div className='id'>{item.id}</div>
						<div className='name'>{item.first_name}</div>
						<div className='email'>{item.email}</div>
					</div>
				))}
			</div>
			<Helmet>
				<title>Modules - Helmet</title>
				<meta name='description' content='helmet Module' />
			</Helmet>
		</div>
	)
}

export default HelmetPage
