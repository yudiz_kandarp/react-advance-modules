import React from 'react'
import { Helmet } from 'react-helmet'
import { useParams } from 'react-router-dom'
import data from '../Data/data_helmet.json'

function SingleUser() {
	const singleUserData = data.find((item) => item.id == useParams().id)
	const { id, first_name, email } = singleUserData
	return (
		<>
			<div className='user_data'>
				<div className='id'>{id}</div>
				<div className='name'>{first_name}</div>
				<div className='email'>{email}</div>
			</div>
			<Helmet>
				<title>hii, {first_name}</title>
				<meta name='description' content={`${first_name} ${email}`} />
			</Helmet>
		</>
	)
}

export default SingleUser
